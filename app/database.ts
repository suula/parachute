import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyCBND8hTbb9HWbfQBw5cwNec3uOObmIURg",
  authDomain: "parachute.firebaseapp.com",
  databaseURL: "https://parachute-3d140.firebaseio.com",
  storageBucket: "parachute-3d140.appspot.com"
};
firebase.initializeApp(config);
const database = firebase.database();
export default database;