import React, { Component } from 'react';
import {
    ListView, Text, ListViewDataSource,
    Image, ImageURISource, View, StyleSheet,
    TouchableHighlight
} from 'react-native';
import { User } from './index';

interface Props {
    users: User[];
    onUserClick: (user: User) => void;
}

interface State {
    dataSource: ListViewDataSource
}

export default class UserList extends Component<Props, State> {

    ds: ListViewDataSource;

    constructor(props: Props) {
        super(props)
        this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    }

    render() {
        const dataSource = this.ds.cloneWithRows(this.props.users);

        return (<ListView
            dataSource={dataSource}
            renderRow={(user: User) =>
                <TouchableHighlight onPress={() => this.props.onUserClick(user)}>
                    <View style={styles.container}>
                        <Image source={require('../img/nopic.png')}
                            style={{ width: 64, height: 64, borderRadius: 32 }} />
                        <View style={styles.innerContainer}>
                            <Text style={{ color: '#fff', fontWeight: 'bold' }}>{user.name}</Text>
                            <Text style={{ color: '#fff' }}>{user.description}</Text>
                            <Text style={{ color: user.isOnline ? '#0f0' : '#f00' }}>{user.isOnline ? 'ONLINE' : 'OFFLINE'}</Text>
                        </View>
                    </View>
                </TouchableHighlight >}
        />);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    innerContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 24
    }
});
