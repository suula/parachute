import React, { Component } from 'react';
import UserList from './UserList';
import { NavigationScreenOptions, NavigationScreenProp, NavigationParams, NavigationAction } from 'react-navigation';
import { User } from './index';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Button,
    Alert,
    ActivityIndicator
} from 'react-native';
import db from './database';

interface Props {
    navigation: NavigationScreenProp<any, any>;
}

interface State {
    msg: string;
    users: User[];
    isLoadingUsers: boolean;
}

export default class ProfilesScreen extends Component<Props, State>{
    static navigationOptions: NavigationScreenOptions = {
        title: 'Profiilit',
    };

    navigate: (routeName: string, params?: NavigationParams, action?: NavigationAction) => boolean;
    userRef: firebase.database.Reference;

    constructor(props: Props) {
        super(props);
        const { navigate } = this.props.navigation;
        this.navigate = navigate;

        this.state = {
            msg: '',
            users: [],
            isLoadingUsers: true
        };

        this.userRef = db.ref('/users');
        this.fetchProfiles = this.fetchProfiles.bind(this);
        this.navigateToProfile = this.navigateToProfile.bind(this);
    }

    componentDidMount() {
        this.listenProfileChanges();
    }

    fetchProfiles() {
        this.setState({ isLoadingUsers: true, users: [] });
        setTimeout(() => {
            this.userRef.once('value')
                .then((snapshot) => {
                    const users = this.parseProfiles(snapshot);
                    this.setState({ isLoadingUsers: false, users: users });
                }).catch((err) => {
                    Alert.alert(err.message);
                    this.setState({ isLoadingUsers: false });
                });
        }, 2000);
    }

    listenProfileChanges() {
        this.setState({ isLoadingUsers: true });
        this.userRef.on('value', (snapshot) => {
            console.log('Fetching data from firebase');
            const users = this.parseProfiles(snapshot);
            this.setState({ isLoadingUsers: false, users: users });
        });
    }

    parseProfiles(snapshot: any): Array<User> {
        const userMap = snapshot != null ? snapshot.val() : {};
        const users: Array<User> = [];
        Object.keys(userMap).forEach(userId => {
            const user: any = userMap[userId];
            users.push({ id: userId, name: user.name, description: user.description, isOnline: user.isOnline });
        });
        return users;
    }

    navigateToProfile(user: User) {
        this.navigate('MyProfile', user);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.brand}>
                    &middot;&middot;&middot;&middot;&middot; Parachute 2017 &middot;&middot;&middot;&middot;&middot;
                </Text>

                <Button
                    title="Lataa profiilit"
                    color="#551A6B"
                    onPress={() => this.fetchProfiles()}></Button>

                {this.state.isLoadingUsers ?
                    (<ActivityIndicator
                        size="large"
                        color="white" />) :
                    (<UserList users={this.state.users} onUserClick={(user: User) => this.navigateToProfile(user)} />)}

                <Button
                    title="Uusi profiili"
                    color="#551A6B"
                    onPress={() => this.navigate('MyProfile')}></Button>

                <Text style={styles.instructions}>
                    Aloita kirjoittamalla jotain tekstilaatikkoon alla
                </Text>
                <View style={styles.input}>
                    <TextInput
                        style={{ width: 280, height: 38 }}
                        keyboardType="default"
                        placeholder="Kirjoita viesti tähän"
                        onChange={(e) => this.setState({ msg: e.nativeEvent.text })}></TextInput>
                </View>
                <Button
                    title="Lähetä"
                    color="#551A6B"
                    onPress={() => Alert.alert('Lähettämäsi viesti', this.state.msg)}></Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#8529A4',
        paddingBottom: 15
    },
    brand: {
        fontSize: 28,
        textAlign: 'center',
        margin: 10,
        color: '#fff'
    },
    instructions: {
        color: '#fff'
    },
    input: {
        backgroundColor: '#fff',
        borderWidth: 5,
        borderColor: '#fff',
        borderRadius: 5,
        margin: 15
    }
});
