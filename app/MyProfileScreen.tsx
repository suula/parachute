import React, { Component } from 'react';
import { NavigationScreenOptions, NavigationScreenProp, NavigationParams, NavigationAction } from 'react-navigation';
import { User } from './index';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Button,
    Alert,
    Switch
} from 'react-native';
import db from './database';

interface Props {
    navigation: NavigationScreenProp<any, any>
}

interface State {
    me: User
}

export default class MyProfileScreen extends Component<Props, State>{
    static navigationOptions: NavigationScreenOptions = {
        title: 'Oma profiilini',
    };

    navigate: (routeName: string, params?: NavigationParams, action?: NavigationAction) => boolean;

    constructor(props: Props) {
        super(props);
        const { navigate, state } = this.props.navigation;
        this.navigate = navigate;
        this.state = { me: state.params || { id: '', name: '', description: '', isOnline: false } }
        this.createProfile = this.createProfile.bind(this);
        this.updateProfile = this.updateProfile.bind(this);
    }

    createProfile() {
        const guid = () => {
            const s4 = () => {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };

        db.ref('users/' + guid()).set({
            name: this.state.me.name,
            description: this.state.me.description,
            isOnline: this.state.me.isOnline
        })
            .then(() => Alert.alert('Profiili luotu', `${this.state.me.name} - ${this.state.me.description} ${this.state.me.isOnline ? 'ONLINE' : 'OFFLINE'}`))
            .catch(err => Alert.alert(err.message));
    }

    updateProfile() {
        db.ref('users/' + this.state.me.id).set({
            name: this.state.me.name,
            description: this.state.me.description,
            isOnline: this.state.me.isOnline
        })
            .then(() => Alert.alert('Profiili tallennettu', `${this.state.me.name} - ${this.state.me.description} ${this.state.me.isOnline ? 'ONLINE' : 'OFFLINE'}`))
            .catch(err => Alert.alert(err.message));
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.brand}>&middot;&middot;&middot;&middot;&middot; Parachute &middot;&middot;&middot;&middot;&middot;</Text>
                <View style={styles.input}>
                    <TextInput
                        style={{ width: 280, height: 38 }}
                        keyboardType="default"
                        placeholder="Nimi"
                        value={this.state.me.name}
                        onChange={(e) => this.setState({ me: Object.assign(this.state.me, { name: e.nativeEvent.text }) })}></TextInput>
                </View>
                <View style={styles.input}>
                    <TextInput
                        style={{ width: 280, height: 38 }}
                        multiline={true}
                        keyboardType="default"
                        placeholder="Kuvaus"
                        value={this.state.me.description}
                        onChange={(e) => this.setState({ me: Object.assign(this.state.me, { description: e.nativeEvent.text }) })}></TextInput>
                </View>
                <View>
                    <Switch
                        onValueChange={(value) => this.setState({ me: Object.assign({}, this.state.me, { isOnline: value }) })}
                        value={this.state.me.isOnline} />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'stretch' }}>
                    <View style={styles.btn}>
                        {this.state.me.id.length > 0 ?
                            (<Button
                                title="Tallenna"
                                color="#551A6B"
                                onPress={() => this.updateProfile()} />) :
                            (<Button
                                title="Luo uusi"
                                color="#551A6B"
                                onPress={() => this.createProfile()} />)}
                    </View>
                </View>
            </View>);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#8529A4',
        paddingBottom: 15
    },
    brand: {
        fontSize: 28,
        textAlign: 'center',
        margin: 10,
        color: '#fff'
    },
    btn: {
        margin: 10
    },
    input: {
        backgroundColor: '#fff',
        borderWidth: 5,
        borderColor: '#fff',
        borderRadius: 5,
        margin: 15
    }
});
