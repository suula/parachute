import { AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';
import ProfilesScreen from './ProfilesScreen'
import MyProfileScreen from './MyProfileScreen'

export interface User {
  id: string;
  name: string;
  description: string;
  isOnline: boolean;
}

const Parachute = StackNavigator({
  Profiles: { screen: ProfilesScreen },
  MyProfile: { screen: MyProfileScreen }
});

AppRegistry.registerComponent('Parachute', () => Parachute);

export default Parachute;